module.exports = function (inventory) {
    let oldCarsArray = [];
    for (let car of inventory) {
        if (car.car_year < 2000) {
            oldCarsArray.push(car.car_year);
        }
    }
    return oldCarsArray.length;
}
