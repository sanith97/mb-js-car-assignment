module.exports = function (inventory) {
    let bmwAndAudiCarsArr = [];
    for (let car of inventory) {
        if ((car.car_make === "Audi") || (car.car_make === "BMW")) { 
            bmwAndAudiCarsArr.push(car.car_model);
        }
    }
    return JSON.stringify(bmwAndAudiCarsArr);
}
