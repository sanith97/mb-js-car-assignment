module.exports = function (inventory) {
    let carModelsArray = [];
    for (let car of inventory) {
        carModelsArray.push(car.car_model);
    }
    carModelsArray.sort();
    return carModelsArray;
}
