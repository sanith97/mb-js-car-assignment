module.exports = function (inventory) {
    let carYearsArray = [];
    for (let car of inventory) {
        carYearsArray.push(car.car_year);
    }
    return carYearsArray;
}
